<?php

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Koehnlein\\KoeClearindex\\Task\\CleanIndexTask'] = array(
    'extension' => 'koe_clearindex',
    'title' => 'Clean Index',
    'description' => 'completely clean index tables of extension "indexed_search"',
);
