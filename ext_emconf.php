<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Clean Index',
    'description' => 'Provides a scheduler task to completely clean index tables of extension "indexed_search"',
    'category' => 'plugin',
    'author' => 'Albrecht Köhnlein',
    'author_email' => 'ak@koehnlein.eu',
    'author_company' => '',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '2.0.1',
    'constraints' => array(
        'depends' => array(
            'indexed_search' => '8.7.0-10.4.99',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
);
