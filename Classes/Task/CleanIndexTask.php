<?php

namespace Koehnlein\KoeClearindex\Task;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

class CleanIndexTask extends AbstractTask
{
    public function execute(): bool
    {
        $tables = ['index_phash', 'index_fulltext', 'index_rel', 'index_words', 'index_section', 'index_grlist', 'index_stat_search', 'index_debug', 'index_config', 'index_stat_word'];
        foreach ($tables as $table) {
            /** @var Connection $connection */
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
            $connection->truncate($table);
        }

        return true;
    }
}
